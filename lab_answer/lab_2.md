1. Apakah perbedaan antara JSON dan XML?
JSON dari segi kompleksitasnya sederhana dan mudah dibaca sedangkan XML lebih rumit.
JSON berorientasi pada data sedangkan XML pada dokumen.
JSON mendukung array namun XML tidak.
File JSON diakhiri dengan ekstensi .json sedangkan file XML diakhiri dengan ekstensi .xml.

2. Apakah perbedaan antara HTML dan XML?
XML singkatan dari eXtensible Markup Language sedangkan HTML singkatan dari Hypertext Markup Language.
XML berfokus pada transfer data sedangkan HTML pada penyajian data.
XML didorong konten sedangkan HTML oleh format.
XML menyediakan dukungan namespaces sedangkan HTML tidak.
XML strict untuk tag penutup sedangkan HTML tidak.
Tag XML dapat dikembangkan sedangkan HTML memiliki tag terbatas.
Tag XML tidak ditentukan sebelumnya sedangkan HTML memiliki tag yang telah ditentukan sebelumnya.

Referensi :
https://blogs.masterweb.com/perbedaan-xml-dan-html/
https://id.sawakinome.com/articles/technology/difference-between-json-and-xml-2.html
