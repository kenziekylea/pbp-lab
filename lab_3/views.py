from django.db.models.fields.related import ForeignKey
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .forms import FriendForm
from .models import Friend
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url="/admin/login/")
def add_friend(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/lab-3/')
    else:
        form = FriendForm()
    return render(request, 'lab3_form.html', {'form': form})