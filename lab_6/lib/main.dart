import 'package:flutter/material.dart';
import 'package:lab_6/product_list_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "PL Page",
      theme: ThemeData(
        // Define the colors
        primaryColor: Colors.amber,

        // Define the default font family.
        fontFamily: 'Brandon',
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: const ProductListPage(),
    );
  }
}
